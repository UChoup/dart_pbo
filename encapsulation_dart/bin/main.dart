import 'dart:io';
import 'package:encapsulation_dart/persegi_panjang.dart'; //untuk memanggil class yang ada di lib

void main(List<String> args) {
  PersegiPanjang kotak1, kotak2; 

  print(kotak1); 

  double luasKotak1;

  kotak1 = new PersegiPanjang(); 
  kotak1.setPanjang(-2); // cara pemakaian method setter = harus dijadikan method dlu
  kotak1.lebar = 3; // cara pemakaian property  setter = sama dengan biasanya

  kotak2 = PersegiPanjang(); 
  print("Masukka Panjang dari Kotak 2 :");
  kotak2.setPanjang(double.tryParse(stdin.readLineSync()));
  print("Masukka Lebar dari Kotak 2 :");
  kotak2.lebar = double.tryParse(stdin.readLineSync());

  luasKotak1 = kotak1.luas; // luas disini untuk memanggil get dari luas properti persegi panjang yang tanpa field

  print("Hasil dari Luas Kotak 1 + Luas Kotak 2 :");
  print(luasKotak1 + kotak2.luas);

  print("ini nilai dari method setPanjang menggunakan method getter : ");
  print(kotak1.getPanjang()); // ini nilai panjang yang sudah kita ambil dari setPanjang

  print("ini nilai dari property set lebar menggunakan property get lebar : ");
  print(kotak1.lebar); // ini nilai panjang yang sudah kita ambil dari property setPanjang

}