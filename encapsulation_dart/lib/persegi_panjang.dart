class PersegiPanjang{ 
  double _panjang; // membuat field panjang manjeadi private dengan "_", sehingga tidak bisa diakses di luar class, biasanya digunakan untuk encapsulation
  double _lebar; 

  //////////////////////////////////////////////////////////////////////////////////////////
  // Menggunakan Method getter setter agar bisa diakses dari luar
  // Method setter untuk merubah nilai, kasus ini kita merubah nilai panjang agar tidak bisa menghasilkan negatif
  void setPanjang(double value){ // double value merupakan nilai yang akan dimasukkan ke panjang
    if (value < 0) { //kita ingin semua yang dimasukkan hasilnya positif 
      value *=-1;
    }
    _panjang = value; // jadi konsepnya kita minta tolong ke setPanjang untuk memasukkan value ke _panjang
  }

  // getter untuk mengambil nilai dari setternya
  double getPanjang(){
    return _panjang;
  }


  // Menggunakan Property getter setter agar bisa diakses dari luar
  // property setter untuk merubah nilai, kasus ini kita merubah nilai lebar agar tidak bisa menghasilkan negatif 
  void set lebar(double value){
    if (value < 0) { //kita ingin semua yang dimasukkan hasilnya positif 
      value *=-1;
    }
    _lebar = value;
  }
  // getter untuk mengambil nilai dari setternya
  double get lebar{
    return _lebar;
  }
  //////////////////////////////////////////////////////////////////////////////////////////
  
  double hitungLuas(){ 
    return this._panjang * this._lebar; 
  }

  // ini properti tanpa field
  double get luas => _panjang * _lebar;
}