// void main(List<String> args) {
//   var count = 10;
//   for (var i = 1; i <= count; i++) {
//     print("$i bismillah");
//   }  
// }

void main(List<String> args) {
  var i = 0;

  // dengan while
  while (i <= 5) {
    print("Bismillah ke - " + i.toString());
    i++;
  }  

  // dengan do while
  do {
    print("Lakukan ini ke - " + i.toString());
    ++i;
  } while (i<=10);
}