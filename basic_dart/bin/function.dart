import 'dart:io';

// ini function biasa
double luas_persegi_panjang(double p, double l, double t){
  return p*l*t;
}

// ini juga function biasa
void sapa_penonton(){
  print('salam all');
}

// Function dengan Optinal Parameter
// Function dgn Name Parameter ditandain dgn {}
String say(String from, String message, {String to, String appName = "Facebook"}){
  return from + " say " + message + ((to != null) ? " to " + to : "") + ((appName != null) ? "Via " + appName : "");
}

// Function dgn positional parameter ditandai dgn [], terdapat juga default param di bagian
String say2(String from, String message, [String to, String appName = "Whatsapp"]){
  return from + " say " + message + ((to != null) ? " to " + to : "") + ((appName != null) ? "Via " + appName : "");
}

//Arrow Expresion
double luas_segiempat(double panjang, double lebar) => panjang * lebar;

// Anonymous Function
// ada di Function operator
int doMathOperator(int number1, int number2, Function(int, int) operator){
  return operator(number1, number2);
}

void main(List<String> args) {
  // Function biasa
  // var panjang = double.tryParse(stdin.readLineSync());  
  // var lebar = double.tryParse(stdin.readLineSync());  
  // var tinggi = double.tryParse(stdin.readLineSync());  
  // var hasil = luas_persegi_panjang(panjang, lebar, tinggi);
  // print("maka hasil dari luas persegi panjang adalah $hasil");
  // sapa_penonton();
  /////////////////////////////////////////////////////////////
  // ini function optional - named parameter
  print(say("Yusuf", "Salam all ", to: "Aji ", appName: "whatsapp"));
  print(say("Yusuf", "Salam all ", to: "Aji "));
  // ini function optional - Optional parameter
  print(say2("Yusuf", "Salam all ", "Wijaya "));
  // panggil arrow expresion
  print(luas_segiempat(10, 20));
  // function juga bisa dijadikan variable
  Function f;
  f = luas_segiempat;
  print(f(10.1, 29.4));
  // panggil anonymous function
  print(doMathOperator(3, 2, (a, b) => a*b));


}