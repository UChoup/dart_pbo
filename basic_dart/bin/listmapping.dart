import 'dart:ffi';

import 'dart:io';

Void main(List<String> args) {
  List<int> list = [1, 2, 3];
  List<int> mylist =[];
  List<int> inilist = [];
  List<int> listsama = [1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 5, 5, 5, 5];
  List<String> liststring = [];

  // function untuk list
  mylist.add(10);
  mylist.addAll(list);
  mylist.insert(1, 20);
  mylist.insertAll(3, [30, 20, 12]);

  // mylist.remove(20);
  // mylist.removeLast();
  // mylist.removeAt(5);
  // mylist.removeRange(0, 3);
  // mylist.removeWhere((number) => number%2 != 0); // maksudnya jika numbernya ganjil maka yang ganjil akan dihapus

  // if (mylist.contains(100)) {
  //   print("ya ada");
  // } else{
  //   print("tidak ada");
  // }
  // list = mylist.sublist(3, 6);

  // list.clear();

  // mylist.sort();
  // mylist.sort((a, b) => b - a);

  // if (mylist.every((element) => element%2 != 0)) {
  //   print("semuanya ganjil"); 
  // } else {
  //   print("tidak semuanya ganjil");
  // }

  // if (inilist.isEmpty) {
  //   print("List ini Kosong");
  // } else {
  //   print("List ini tidak kosong");
  // }
  // if (inilist.isNotEmpty) {
  //   print("List ini tidak Kosong");
  // } else {
  //   print("List ini kosong");
  // }

  // Set<int> s; // set itu unik artinya tidak boleh sama isinya
  // s = listsama.toSet();

  liststring = listsama.map((number) => "angka" + number.toString()).toList(); // menggunakan map untuk mengubah int ke string
  liststring.forEach((str) {
    print(str);
  });

  for (var i in mylist) {
     print(i);
   }




  // //cara menggunakan list
  // list[0] = 10;
  // int e = list[0];
  // print(e);

  // // dengan perulangan
  // for (var i = 0; i < list.length; i++) {
  //   print(list[i]);
  // }

  // print("==========");

  // for (var i in list) {
  //   print(i);
  // }

  // print("==========");

  // list.forEach((bilangan) {
  //   print(bilangan);
  // });
}