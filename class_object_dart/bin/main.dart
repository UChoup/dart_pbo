import 'dart:io';

void main(List<String> args) {
  PersegiPanjang kotak1, kotak2; // ini indentifier dari class persegi panjang

  print(kotak1); // akan menghasilkan null karena baru ada indentifiernya saja

  double luasKotak1;

  kotak1 = new PersegiPanjang(); // ini object nya yang diambil dari rancangan class persegipanjang mengguakan new
  kotak1.panjang = 2; //ini inputnya
  kotak1.lebar = 3; 

  kotak2 = PersegiPanjang(); // untuk objectnya dart menyarankan tidak perlu memakai new
  kotak2.panjang = double.tryParse(stdin.readLineSync());
  kotak2.lebar = double.tryParse(stdin.readLineSync());

  luasKotak1 = kotak1.hitungLuas();

  print(luasKotak1 + kotak2.hitungLuas());


  

  

}

class PersegiPanjang{ // ini class
  double panjang; // ini field nya = segala sesuatu yang dimiliki class
  double lebar; // ini fieldnya

  double hitungLuas(){ //ini method/function = apa yang bisa dilakukan oleh class
    return this.panjang * this.lebar; // this ini melambangkan field yg ada di class 
  }
}