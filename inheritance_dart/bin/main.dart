import 'dart:io';

import 'package:inheritance_dart/hero.dart';
import 'package:inheritance_dart/monster.dart';
import 'package:inheritance_dart/monster_kecoa.dart';
import 'package:inheritance_dart/monster_ubur_ubur.dart';

main(List<String> arguments) async {
  Hero h = Hero();
  Monster m = Monster(); //indentifier
  MonsterUburUbur u = MonsterUburUbur();
  List<Monster> monsters = [];

  monsters.add(MonsterUburUbur());
  monsters.add(MonsterKecoa());
  monsters.add(MonsterUburUbur());

  print((m as MonsterUburUbur).swim()); // as digunakan untuk menspesifikkan agar method yang ada di monster ubur ubur dapat muncul

  for (Monster m in monsters) {
    if (m is MonsterUburUbur) { // is digunakan untuk menverifikasi bahwa m nya ke araha Monster ubur ubur
      print(m.swim());
    }
  }

  h.healtPoint = -10;
  m.healtPoint = 20;
  u.healtPoint = 3;

  print("Hero HP :"  + h.healtPoint.toString());
  print("Monster HP : " + m.healtPoint.toString());
  print("Monster Ubur Ubur HP : " + u.healtPoint.toString());
  print(h.killAMonster());
  print(m.eatHuman());
  print(u.swim());
  print(u.eatHuman());
}