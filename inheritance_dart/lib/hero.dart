import 'package:inheritance_dart/character.dart';

class Hero extends Character{ // class extends ini merupakan turunan/inheritance dari Character sehingga dia bisa memanggil class yang ada di character
  String killAMonster() => "Serangggg !!!! Matii !!!!";
}